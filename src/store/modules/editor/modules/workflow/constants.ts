/*****************************************************************************
 * Copyright (C) 2018 VideoLAN
 *
 * Authors: Alper Çakan <alpercakan98@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

export const _SET_CURRENT_WORKFLOW_TIME = "setCurrentWorkflowTime";
export const SET_CURRENT_WORKFLOW_TIME = "editor/workflow/setCurrentWorkflowTime";

export const _REGISTER_SEEK_LISTENER = "registerSeekListener";
export const REGISTER_SEEK_LISTENER = "editor/workflow/registerSeekListener";

export const _SET_TRACKS = "setTracks";
export const SET_TRACKS = "editor/workflow/setTracks";

export const _SET_FPS = "setFps";
export const SET_FPS = "editor/workflow/setFps";

export const _SET_CLIPS = "setClips";
export const SET_CLIPS = "editor/workflow/setClips";

export const _SET_LENGTH = "setLength";
export const SET_LENGTH = "editor/workflow/setLength";

export const _ADD_EFFECT_INSTANCE = "addEffectInstance";
export const ADD_EFFECT_INSTANCE = "editor/workflow/addEffectInstance";

export const _ADD_CLIP_INSTANCE = "addClipInstance";
export const ADD_CLIP_INSTANCE = "editor/workflow/addClipInstance";

export const _REMOVE_CLIP_INSTANCE = "removeClipInstance";
export const REMOVE_CLIP_INSTANCE = "editor/workflow/removeClipInstance";

export const _SET_CLIP_INSTANCES = "setClipInstances";
export const SET_CLIP_INSTANCES = "editor/workflow/setClipInstances";
