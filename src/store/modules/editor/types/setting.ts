/*****************************************************************************
 * Copyright (C) 2018 VideoLAN
 *
 * Authors: Alper Çakan <alpercakan98@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

export interface ISetting {
  name: string;
  key: string;
  description: string;
}

export interface IBooleanSetting extends ISetting { }

export enum NumericSettingType {
  DOUBLE = "DOUBLE",
  INTEGER = "INTEGER",
}

export interface INumericSetting extends ISetting {
  numericType: NumericSettingType;
  min: number;
  max: number;
}

export interface ISettingValue {
  setting: ISetting;
  value: any;
}

export interface IBooleanSettingValue extends ISettingValue {
  setting: IBooleanSetting;
  value: boolean;
}

export interface INumericSettingValue extends ISettingValue {
  setting: INumericSetting;
  value: number;
}
