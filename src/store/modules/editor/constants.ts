/*****************************************************************************
 * Copyright (C) 2018 VideoLAN
 *
 * Authors: Alper Çakan <alpercakan98@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

export const _SET_MEDIA_LIBRARY_VISIBILITY = "setMediaLibraryVisibility";
export const SET_MEDIA_LIBRARY_VISIBILITY = "editor/setMediaLibraryVisibility";

export const _SET_CLIP_LIBRARY_VISIBILITY = "setClipLibraryVisibility";
export const SET_CLIP_LIBRARY_VISIBILITY = "editor/setClipLibraryVisibility";

export const _SET_CLIP_PREVIEW_VISIBILITY = "setClipPreviewVisibility";
export const SET_CLIP_PREVIEW_VISIBILITY = "editor/setClipPreviewVisibility";

export const _SET_PROJECT_PREVIEW_VISIBILITY = "setProjectVisibilityPreview";
export const SET_PROJECT_PREVIEW_VISIBILITY = "editor/setProjectVisibilityPreview";

export const _SET_EFFECTS_LIST_VISIBILITY = "setEffectsListVisibility";
export const SET_EFFECTS_LIST_VISIBILITY = "editor/setEffectsListVisibility";

export const _SET_TRANSITIONS_LIST_VISIBILITY = "setTransitionsListVisibility";
export const SET_TRANSITIONS_LIST_VISIBILITY = "editor/setTransitionsListVisibility";

export const _SET_CURRENT_CLIP_TIME = "setCurrentClipTime";
export const SET_CURRENT_CLIP_TIME = "editor/setCurrentClipTime";

export const _SET_LOADING = "setLoading";

export const _SET_ERROR = "setError";

export const _SET_AVAILABLES_TRANSITIONS_LIST = "setAvailableTransitionsList";

export const _SET_AVAILABLES_EFFECTS_LIST = "setAvailableEffectsList";
