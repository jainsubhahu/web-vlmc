/*****************************************************************************
 * Copyright (C) 2018 VideoLAN
 *
 * Authors: Alper Çakan <alpercakan98@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

// Duration Format

export default (ms: number, includeSplitSecond: boolean = false) => {
  const splitSeconds = ms / 10;
  const seconds = splitSeconds / 100;
  const minutes = seconds / 60;
  const hours = minutes / 60;

  const floorAndPad = (x: number) => String(Math.floor(x)).padStart(2, "0");

  const basic = `${floorAndPad(hours)}:${floorAndPad(minutes % 60)}:${floorAndPad(seconds % 60)}`;

  if (includeSplitSecond) {
    return basic + ":" + floorAndPad(splitSeconds % 100);
  }

  return basic;
};
