/*****************************************************************************
 * Copyright (C) 2018 VideoLAN
 *
 * Authors: Alper Çakan <alpercakan98@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

import { DRAG_FORMAT_CLIP } from "@/constants/drag-and-drop";
import Thumbnail from "@/elements/Thumbnail/Thumbnail.vue";
import Window from "@/elements/Window/Window.vue";
import { _SET_CLIP_LIBRARY_VISIBILITY } from "@/store/modules/editor/constants";
import { IClip } from "@/store/modules/editor/types/clip";
import durationFormat from "@/utils/duration-format";
import Vue from "vue";
import { mapGetters, mapMutations, mapState } from "vuex";

export default Vue.extend({
  components: {
    Thumbnail,
    Window,
  },

  data() {
    return {

    };
  },

  computed: {
    ...mapState("editor/workflow", ["clips"]),
    ...mapGetters("editor/mediaLibrary", ["getMediumById"]),
  },

  methods: {
    ...mapMutations("editor", [_SET_CLIP_LIBRARY_VISIBILITY]),

    hideClipLibrary() {
      this[_SET_CLIP_LIBRARY_VISIBILITY]({newVisibility: false});
    },

    getClipThumbnail(clip: IClip) {
      const parentMedium = this.getMediumById(clip.parentMediumId);

      return parentMedium ? parentMedium.thumbnail : null;
    },

    getClipName(clip: IClip) {
      const parentMedium = this.getMediumById(clip.parentMediumId);

      return parentMedium ? parentMedium.name : null;
    },

    getClipLength(clip: IClip) {
      return clip.end - clip.begin;
    },

    displayLength(ms: number) {
      return durationFormat(ms);
    },

    handleItemDragStart(clip: IClip, event: any) {
      event.dataTransfer.setData(DRAG_FORMAT_CLIP,
                                 clip.id);
    },
  },
});
