/*****************************************************************************
 * Copyright (C) 2018 VideoLAN
 *
 * Authors: Alper Çakan <alpercakan98@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

import { DRAG_FORMAT_EFFECT, DRAG_FORMAT_TRANSITION } from "@/constants/drag-and-drop";
import Window from "@/elements/Window/Window.vue";
import { _SET_EFFECTS_LIST_VISIBILITY,
  _SET_TRANSITIONS_LIST_VISIBILITY } from "@/store/modules/editor/constants";
import { IEffect } from "@/store/modules/editor/types/effect";
import { ITransition } from "@/store/modules/editor/types/transition";
import Vue from "vue";
import { mapMutations, mapState } from "vuex";

export default Vue.extend({
  components: {
    Window,
  },

  props: {
    transitionsMode: {
      default: false,
      type: Boolean,
    },
  },

  data() {
    return {};
  },

  computed: {
    ...mapState("editor", ["availableEffects", "availableTransitions"]),

    title() {
      return this.transitionsMode ? "Transitions List" : "Effects List";
    },

    list() {
      return this.transitionsMode ? this.availableTransitions : this.availableEffects;
    },
  },

  methods: {
    ...mapMutations("editor", [_SET_EFFECTS_LIST_VISIBILITY, _SET_TRANSITIONS_LIST_VISIBILITY]),

    hideEffectsTransitionsList() {
      this[this.transitionsMode ? _SET_TRANSITIONS_LIST_VISIBILITY : _SET_EFFECTS_LIST_VISIBILITY]
        ({newVisibility: false});
    },

    handleItemDragStart(effectOrTransititon: IEffect | ITransition, event: any) {
      event.dataTransfer.setData(this.transitionsMode ? DRAG_FORMAT_TRANSITION : DRAG_FORMAT_EFFECT,
                                 effectOrTransititon.name);
    },
  },
});
