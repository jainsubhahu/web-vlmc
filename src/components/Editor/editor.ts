/*****************************************************************************
 * Copyright (C) 2018 VideoLAN
 *
 * Authors: Alper Çakan <alpercakan98@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

import ClipLibrary from "@/components/ClipLibrary/ClipLibrary.vue";
import EffectsTransitionsList from "@/components/EffectsTransitionsList/EffectsTransitionsList.vue";
import MediaLibrary from "@/components/MediaLibrary/MediaLibrary.vue";
import Preview from "@/components/Preview/Preview.vue";
import Timeline from "@/components/Timeline/Timeline.vue";
import Overlay from "@/elements/Overlay/Overlay.vue";
import SpringSpinner from "epic-spinners/src/components/lib/SpringSpinner.vue";
import Vue from "vue";
import { mapActions, mapState } from "vuex";

export default Vue.extend({
  components: {
    ClipLibrary,
    EffectsTransitionsList,
    MediaLibrary,
    Overlay,
    Preview,
    SpringSpinner,
    Timeline,
  },

  data() {
    return {};
  },

  mounted() {
    this.connect();
  },

  computed: {
    ...mapState("editor",
                  ["mediaLibraryVisible",
                   "clipLibraryVisible",
                   "clipPreviewVisible",
                   "projectPreviewVisible",
                   "effectsListVisible",
                   "transitionsListVisible",
                   "loading",
                   "error"]),
  },

  methods: {
    ...mapActions("editor", ["connect"]),
  },
});
