/*****************************************************************************
 * Copyright (C) 2018 VideoLAN
 *
 * Authors: Alper Çakan <alpercakan98@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

import { DRAG_FORMAT_CLIP, DRAG_FORMAT_EFFECT, DRAG_FORMAT_TRANSITION } from "@/constants/drag-and-drop";
import Button from "@/elements/Button/Button.vue";
import SeekBar from "@/elements/SeekBar/SeekBar.vue";
import { SET_CURRENT_WORKFLOW_TIME } from "@/store/modules/editor/modules/workflow/constants";
import { IClipInstance } from "@/store/modules/editor/types/clip-instance";
import { InputFormat } from "@/store/modules/editor/types/input-format";
import { ITrack } from "@/store/modules/editor/types/track";
import durationFormat from "@/utils/duration-format";
import timeToPercentage from "@/utils/time-to-percentage";
import Vue from "vue";
import { mapActions, mapGetters, mapMutations, mapState } from "vuex";

const FRAME_COUNT_TO_PIXEL = 1 / 10;
const PIXEL_TO_FRAME_COUNT = 1 / FRAME_COUNT_TO_PIXEL;
const TIME_PADDING = 1800;

export default Vue.extend({
  components: {
    Button,
    SeekBar,
  },

  data() {
    return {
      cursorPos: 0,
      timePadding: TIME_PADDING,
    };
  },

  computed: {
    ...mapState("editor/workflow",
      ["currentTimeMs", "length", "audioTracks", "videoTracks"]),
    ...mapGetters("editor/workflow", ["getClipInstancesOnTrack",
                                      "getClipFromInstance",
                                      "getEffectsOnClip"]),
    ...mapGetters("editor/mediaLibrary", ["getMediumById"]),

    width() {
      return `${(this.$store.state.editor.workflow.length + TIME_PADDING) * FRAME_COUNT_TO_PIXEL}px`;
    },

    displayTime() {
      return durationFormat(this.currentTimeMs, true);
    },

    displayedTracks() {
      const clipWithInfoFn = (clipInstance: IClipInstance) => {
        const clip = this.getClipFromInstance(clipInstance);
        const medium = this.getMediumById(clip.parentMediumId);
        const clipLength = clip.end - clip.begin;

        return {
          ...clipInstance,
          hasEffect: this.getEffectsOnClip(clipInstance).length > 0,
          name: medium.name,
          trackLengthPercentage: `${timeToPercentage(clipLength, this.length + TIME_PADDING)}%`,
          trackPosPercentage: `${timeToPercentage(clipInstance.position, this.length + TIME_PADDING)}%`,
       };
      };

      const trackWithInfoFn = (track: ITrack, index: number) => {
        const displayedTrackClips = this.getClipInstancesOnTrack(track).map(clipWithInfoFn);
        const isVideo = track.format === InputFormat.VIDEO;

        return {
          ...track,
          displayedTrackClips,
          index: index + 1,
          isVideo,
          lastVideo: isVideo && index === 0,
        };
      };

      const videoTracks = this.videoTracks
                            .slice(0, this.lastDisplayedTrackIndex(this.videoTracks) + 1)
                            .map(trackWithInfoFn)
                            .reverse();
      const audioTracks = this.audioTracks
                            .slice(0, this.lastDisplayedTrackIndex(this.audioTracks) + 1)
                            .map(trackWithInfoFn);

      return [...videoTracks,
              ...audioTracks];
    },
  },

  methods: {
    ...mapActions("editor/workflow", ["addEffectInstance", "addClipInstance", "removeClipInstance"]),

    handleSeekClick(event: { to: number }) {
      this.$store.commit(SET_CURRENT_WORKFLOW_TIME,
        { newTime: event.to });
    },

    handleSeekBarCursorMove(event: { percentage: number }) {
      this.cursorPos = `${event.percentage}%`;
    },

    getTrackFormatString(track: ITrack) {
      return track.format === InputFormat.VIDEO ? "Video" : "Audio";
    },

    handleDropOnClip(trackClip: { id: string }, event: { dataTransfer: DataTransfer }) {
      const effectName = event.dataTransfer.getData(DRAG_FORMAT_EFFECT);

      if (!effectName) {
        return;
      }

      this.addEffectInstance({ clipId: trackClip.id, effectName });
    },

    handleDropOnTrack(track: { id: string }, event: { dataTransfer: DataTransfer, offsetX: number }) {
      const clipId = event.dataTransfer.getData(DRAG_FORMAT_CLIP);

      if (!clipId) {
        return;
      }

      this.addClipInstance({
        clipId,
        pos: event.offsetX * PIXEL_TO_FRAME_COUNT,
        trackId: track.id,
      });
    },

    removeClip(trackClip: { id: string }) {
      this.removeClipInstance({ id: trackClip.id });
    },

    lastDisplayedTrackIndex(tracks: ITrack[]) {
      for (let i = tracks.length - 1; i >= 0; --i) {
        if (this.getClipInstancesOnTrack(tracks[i]).length !== 0) {
          return i + 1;
        }
      }

      return 0;
    },
  },
});
