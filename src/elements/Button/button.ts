/*****************************************************************************
 * Copyright (C) 2018 VideoLAN
 *
 * Authors: Alper Çakan <alpercakan98@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

import * as classNames from "classnames";
import Vue from "vue";

export default Vue.extend({
  props: {
    bordered: Boolean,
    colorScheme: {
      default: "normal",
      type: String,
    },
    disabled: Boolean,
    icon: String,
    round: Boolean,
    size: {
      default: "small",
      type: String,
    },
   },

  data() {
    return {};
  },

  computed: {
    iconFilePath() {
      return require(`@/assets/${this.icon}.svg`);
    },

    classes() {
      return classNames(
        { round: this.round },
        this.colorScheme,
        { bordered: this.bordered },
        this.size,
        { disabled: this.disabled },
      );
    },
  },

  methods: {
    handleClick() {
      this.$emit("click");
    },
  },
});
