/*****************************************************************************
 * Copyright (C) 2018 VideoLAN
 *
 * Authors: Alper Çakan <alpercakan98@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

import durationFormat from "@/utils/duration-format";
import timeToPercentage from "@/utils/time-to-percentage";
import Vue from "vue";

const SHORT_BACKGROUND_IMAGE_PATH = require(`@/assets/short-ruler.svg`); // tslint:disable-line
const LONG_BACKGROUND_IMAGE_PATH = require(`@/assets/long-ruler.svg`); // tslint:disable-line
const CARET_IMAGE_PATH = require(`@/assets/caret.svg`); // tslint:disable-line
const SHORT_RULER_SCALE_CONSTANT = 6000000;
const LONG_RULER_SCALE_CONSTANT = 120000;
const BAR_LINE_SCALE_CONSTANT = 200000;

export default Vue.extend({
  props: {
    duration: { // milliseconds
      required: true,
      type: Number,
    },

    timePadding: { // milliseconds
      default: 0,
      type: Number,
    },

    currentTime: { // milliseconds
      required: true,
      type: Number,
    },

    lazy: { // if true, "seek" event will be thrown only when it is over
      default: false,
      type: Boolean,
    },

    markerAPos: {
      default: -1,
      type: Number,
    },

    markerBPos: {
      default: -1,
      type: Number,
    },

    longMode: {
      default: false,
      type: Boolean,
    },

    caretCursor: {
      default: false,
      type: Boolean,
    },

    tooltip: {
      default: true,
      type: Boolean,
    },
  },

  data() {
    return {
      currentMousePosition: 0,
      currentWidth: 0,
      mouseDown: false,
      mouseIn: false,
      mouseMovedAfterDown: false,
    };
  },

  created() {
    window.addEventListener("mousemove", this.handleMouseMove);
    window.addEventListener("mouseup", this.handleMouseUp);
  },

  beforeDestroy() {
    window.removeEventListener("mousemove", this.handleMouseMove);
    window.removeEventListener("mouseup", this.handleMouseUp);
  },

  computed: {
    barDuration() {
      return this.timePadding + this.duration;
    },

    barLineWidth() {
      return `${BAR_LINE_SCALE_CONSTANT / this.barDuration}%`;
    },

    containerStyle() {
      return {
        backgroundImage: `url(${this.longMode ? LONG_BACKGROUND_IMAGE_PATH : SHORT_BACKGROUND_IMAGE_PATH})`,
        backgroundSize:
        `${(this.longMode ? LONG_RULER_SCALE_CONSTANT : SHORT_RULER_SCALE_CONSTANT) / this.barDuration}% 100%`,
      };
    },

    cursorPos() {
      const percentage = this.videoPosToBarPosPercentage(this.currentTime);

      this.$emit("cursorMove", { percentage });
      return `${percentage}%`;
    },

    caretStyle() {
      return { backgroundImage: `url(${CARET_IMAGE_PATH }`,
               left: `calc(${this.cursorPos} - 4px)`};
    },

    markerABarPos() {
      return `${this.videoPosToBarPosPercentage(this.markerAPos)}%`;
    },

    markerBBarPos() {
      return `${this.videoPosToBarPosPercentage(this.markerBPos)}%`;
    },

    showTooltip() {
      return this.tooltip && this.mouseIn && this.currentMousePosition >= 0;
    },

    tooltipContent() {
      return durationFormat(this.barDuration * this.currentMousePosition / this.currentWidth, true);
    },
  },

  methods: {
    seek(pos: number) {
      pos = Math.min(Math.max(0, pos), this.duration);

      this.$emit("seek", { to: pos });
    },

    barPosToVideoPos(x: number) {
      return x / this.$refs.container.getBoundingClientRect().width * this.barDuration;
    },

    videoPosToBarPosPercentage(ms: number) {
      return timeToPercentage(ms, this.barDuration);
    },

    handleClick(e: MouseEvent) {
      if (this.lazy || !this.mouseMovedAfterDown) {
        this.seek(this.barPosToVideoPos(this.currentMousePosition));
      }
    },

    handleMouseDown(e: MouseEvent) {
      if (e.button !== 0) {
        return;
      }

      this.mouseDown = true;
      this.mouseMovedAfterDown = false;
    },

    handleMouseUp(e: MouseEvent) {
      if (e.button !== 0) {
        return;
      }

      this.mouseDown = false;
    },

    handleMouseLeave() {
      this.mouseIn = false;
    },

    handleMouseEnter() {
      this.mouseIn = true;
    },

    handleMouseMove(e: MouseEvent) {
      this.mouseMovedAfterDown = true;

      this.currentMousePosition = e.clientX - this.$refs.container.getBoundingClientRect().x;
      this.currentWidth = this.$refs.container.getBoundingClientRect().width;

      if (!this.lazy && this.mouseDown) {
        this.seek(this.barPosToVideoPos(this.currentMousePosition));
      }
    },
  },
});
