/*****************************************************************************
 * Copyright (C) 2018 VideoLAN
 *
 * Authors: Alper Çakan <alpercakan98@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

import Vue from "vue";

const DEFAULT_THUMBNAIL = require(`@/assets/vlmc.png`); // tslint:disable-line

export default Vue.extend({
  props: {
    aspectRatio: {
      default: 16 / 9 ,
      type: Number,
    },

    thumbnail: {
      type: String,
    },
  },

  data() {
    return {};
  },

  computed: {
    style() {
      return {
        backgroundImage: `url(${this.thumbnail || DEFAULT_THUMBNAIL})`,
        paddingTop: `${100 / this.aspectRatio}%`,
      };
    },
  },

  methods: {

  },
});
